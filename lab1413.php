<?php
// find student by nickname in the database
function find_student($username, $students) {
    if(isset($students[$username])) {  // determine if the nickname is set and is not NULL
        return $students[$username]; //  nickname exists into the database , the function return array with all information about student
    } else {
        echo "No such user!"; // if nickname doesn't exist then this message is displayed
        return false;   // function return false
    }
}
// display information about a student
function get_student_info_string($students_info, $username){
  echo "Information about student with nickname '{$username}': \n";
    echo "Name: {$students_info['name']}\nAge: {$students_info['age']}\nGender: {$students_info['gender']} \nLessons:";
   echo implode(', ', $students_info['lesson']);
}

// database of students
$students = [
    'peter' => [
        'name' => "Peter",
        'age' => 25,
        'gender'=> "male",
        'lesson' => [],
    ],
    'vasya' => [
        'name' => "Vasya",
        'age' => 20,
        'gender' => "male",
        'lesson' => [],
    ],
    'iren' => [
        'name' => "Ira",
        'age' => 21,
        'gender'=> "female",
        'lesson' => [],
    ]
];
// array with programming languages list
$languages = [
    'PHP',
    'Python',
    'Java',
    'C',
    'C#',
    'C++',
    'Ruby',
    'Assembler',
    'JS',
];
// add into each lesson array some quantity of programming languages
$students['peter']['lesson'] = [$languages[1], $languages[6], $languages[2]];
$students['vasya']['lesson'] = [$languages[0], $languages[1], $languages[8]];
$students['iren']['lesson'] = [$languages[3], $languages[5], $languages[7]];

$username = isset($argv[1]) ? $argv[1] : ''; // nickname passed from command line
$students_info = find_student($username,$students); // check nickname and return all information about this student

if (boolval($students_info)) { // if nickname exists then call function get_student_info_string for display all information about student
    get_student_info_string($students_info, $username);
}
