<?php
$array = range(1, 100);  // create array
shuffle($array);  // shuffle array
$min = 25;
$max = 32;
// Filters elements of an array using a callback function   'array_filter'
$filered_array = array_filter($array, function($value) use ($min, $max){
    if($value > $min && $value < $max) {
        return true;
    }
    return false;
});
echo "\n Array from {$min} to {$max}: ".implode(", ",$filered_array); // Print  filtered   array

// Student should to build each value of filtered array in the second degree
$func = function($n)  // Anonymous functions for count second degree
{
    return ($n*$n);
};
$double_array = array_map($func, $filered_array); // Call anonymous functions for count second degree
echo "\n Array from {$min} to {$max} in the second degree :".implode(", ",$double_array);  // Print  array with elements in the second degree


