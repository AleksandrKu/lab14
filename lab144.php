<?php
/** Student should create application for search employee with some special skills in employees array and print employees’ data
 * Created application should accept next conditions:
 * o If employee has no required skills application should stop current employee proceed and jump to the next employee in the list
 * o If employee has required skills application should print information about founded employee and stop checking next employees
 */
// array with employees and their skills
$employees = [
    [
        'name' => 'Clark Kent',
        'age' => 22,
        'skills' => ['PHP', 'Java', 'C#']
    ],
    [
        'name' => 'Steve Stifler',
        'age' => 21,
        'skills' => ['PHP', 'JS', 'CSS', 'HTML']
    ],
    [
        'name' => 'Bruce Wayne',
        'age' => 35,
        'skills' => ['PHP', 'PHP Unit', 'XDebug', 'JS']
    ],
    [
        'name' => 'Peter Parker',
        'age' => 18,
        'skills' => ['PHP', 'C', 'Pascal']
    ]
];
$requiredSkillsInput = $argv; // Array of arguments passed from a console to a script
unset($requiredSkillsInput[0]);  // unset 0 argument
if (isset($requiredSkillsInput[1])) {  // if was entered skill then start script
    echo "You need skill : ".$skillInput = $requiredSkillsInput[1]; // Print entered  skill

    foreach ($employees as $emloyeesInLoop) {  //  iterate over arrays
        if (in_array($skillInput, $emloyeesInLoop['skills']) == true) {  // Checks if a value of $skillInput exists in the array
            echo "\n{$emloyeesInLoop['name']}\nAge - {$emloyeesInLoop['age']} years\n"; // print information about founded employee
            $skills = implode(', ', $emloyeesInLoop['skills']);  // Join array elements in  a string  separated comma
            echo "Skills: {$skills}"; // print skills which employee has
            break;  //  ends execution  of the loop foreach
        }
    }
    if (!isset($skills)) { // if $skills  empty that is mean that the database does not have employees with this skills
        echo "\nWe don't have employee, with '{$skillInput}' skill.";
    }
} else {   // if doesn't enter skill then print message and script finished
    echo "You didn't enter a skill. Enter skill wich you need!";
}
