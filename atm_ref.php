<?php
$requestByConsumerIn = $argv; // Withdrawal amount entered in the console
 // Set up quantities of notes in the ATM
$arrayNotesInAtm = array(
    '100' => 10,
    '50' => 20,
    '20' => 50,
    '10' => 100,
);
// Calculation of the amount of money in the ATM
$amountOfMoneyInAtm = $arrayNotesInAtm['100'] * 100 + $arrayNotesInAtm['50'] * 50 + $arrayNotesInAtm['20'] * 20 + $arrayNotesInAtm['10'] * 10;

$arrayNumberNotesForOutput = array(
    '100' => 0,
    '50' => 0,
    '20' => 0,
    '10' => 0,
);

function checkErrors($requestByConsumer, $amountOfMoneyInAtm)
{
    if (!is_numeric($requestByConsumer)) {  // Check: is it number?
        $checkErrors = "\nEnter only numbers!";
        return $checkErrors;
    }
    $arr1 = str_split($requestByConsumer);
    if ($arr1[0] == '-') {  // Check: is the first character '-' ?
        $checkErrors = "\nEnter only numbers without '-'!";
            return $checkErrors;
        }
    if ($requestByConsumer > $amountOfMoneyInAtm) { // Amount of money to give more than in the ATM
        $checkErrors = "\nNot enough cash in the ATM!\nIn the ATM = {$amountOfMoneyInAtm}";
        return $checkErrors;
    } elseif ($requestByConsumer % 10 != 0) {  // Amount of money is not a multiple of 10
        $checkErrors = "\nEnter withdrawal amount in multiples of 10!\n";
        return $checkErrors;
    }
    $ok = "ok";
    return $ok;
}

//  Calculation  number of notes to give
function countNotesForOutput($requestByConsumer,$arrayNotesInAtm)
{
   // $numbers = func_get_args(); //  Receive array with the number of notes in the ATM
    echo "\nNumber of 100grn. = " . $arrayNotesInAtm['100'];
    echo "\nNumber of 50grn. = " . $arrayNotesInAtm['50'];
    echo "\nNumber of 20grn. = " . $arrayNotesInAtm['20'];
    echo "\nNumber of 10grn. = " . $arrayNotesInAtm['10'];
    $arrayNumberNotesForOutput['100'] = 0;
    $arrayNumberNotesForOutput['50'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.
    $arrayNumberNotesForOutput['20'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.
    $arrayNumberNotesForOutput['10'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.

    // Calculation  how many notes by 100grn to need
    $arrayNumberNotesForOutput['100'] = intval($requestByConsumer / 100);
    $arrayNotesInAtm['100'] = $arrayNotesInAtm['100'] - $arrayNumberNotesForOutput['100']; // How many notes by 100grn left in ATM
    $remainder = intval($requestByConsumer % 100);  //  Remainder. Shows amount many left to give
    // If banknotes by 100grn are over the remainder is increased by this amount
    if ($arrayNotesInAtm['100'] < 0) {
        $remainder = $remainder - $arrayNotesInAtm['100'] * 100;
        $arrayNumberNotesForOutput['100'] = $arrayNumberNotesForOutput['100'] + $arrayNotesInAtm['100']; // Count the number of notes to give
    }
    // Calculation  how many notes by 50grn to need
    if ($remainder != 0) {
        $arrayNumberNotesForOutput['50'] = intval($remainder / 50);
        $arrayNotesInAtm['50'] = $arrayNotesInAtm['50'] - $arrayNumberNotesForOutput['50'];
        $remainder = intval($remainder % 50);
        if ($arrayNotesInAtm['50'] < 0) {
            $remainder = $remainder - $arrayNotesInAtm['50'] * 50;
            $arrayNumberNotesForOutput['50'] = $arrayNumberNotesForOutput['50'] + $arrayNotesInAtm['50'];
        }
        // Calculation  how many notes by 20grn to need
        if ($remainder != 0) {
            $arrayNumberNotesForOutput['20'] = intval($remainder / 20);
            $arrayNotesInAtm['20'] = $arrayNotesInAtm['20'] - $arrayNumberNotesForOutput['20'];
            $remainder = intval($remainder % 20);
            if ($arrayNotesInAtm['20'] < 0) {
                $remainder = $remainder - $arrayNotesInAtm['20'] * 20;
                $arrayNumberNotesForOutput['20'] = $arrayNumberNotesForOutput['20'] + $arrayNotesInAtm['20'];
            }
            // Calculation  how many notes by 10grn to need
            if ($remainder != 0) {
                $arrayNumberNotesForOutput['10'] = intval($remainder / 10);
                echo PHP_EOL;
                $remainder = intval($remainder % 10);
                echo $remainder != 0 ? "Error!  Enter other number" : " ";
            }
        }

}
    return $arrayNumberNotesForOutput;
}

// Count how many notes left in the ATM
function leftNoutesInATM($inATM, $outFromATM)
{
    return $inATM - $outFromATM;
}
//  -------------------  MAIN -----------------------------------
// Check:  Withdrawal amount is defined
if (!empty($requestByConsumerIn[1])) {
    $requestByConsumer = $requestByConsumerIn[1];
    echo "Withdrawal amount : {$requestByConsumer}";
} else {
    $requestByConsumer = "";
    echo "Entered withdrawal amount!";  // If withdrawal amount empty
}

if (checkErrors($requestByConsumer, $amountOfMoneyInAtm) == 'ok') { // check errors
    $requestByConsumer = intval($requestByConsumer);
    $arrayNumberNotesForOutput = countNotesForOutput($requestByConsumer, $arrayNotesInAtm);  //  Calculation number of notes to give
    $arrayLeftNoutesInATM = array_map("leftNoutesInATM", $arrayNotesInAtm, $arrayNumberNotesForOutput); // Count how many notes left in the ATM
// Display result
    echo <<<ENDD
    
In the ATM = {$amountOfMoneyInAtm}

ATM must gives:
100grn - $arrayNumberNotesForOutput[100] notes
50grn - $arrayNumberNotesForOutput[50] notes
20grn - $arrayNumberNotesForOutput[20] notes
10grn - $arrayNumberNotesForOutput[10] notes

Left in ATM:
100grn - $arrayLeftNoutesInATM[0] notes
50grn - $arrayLeftNoutesInATM[1] notes
20grn - $arrayLeftNoutesInATM[2] notes
10grn - $arrayLeftNoutesInATM[3] notes
ENDD;
} else {
    echo checkErrors($requestByConsumer, $amountOfMoneyInAtm);  // Entered number with errors
}