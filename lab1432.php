<?php
// Student should create script which will generate random boolean value and check it:
// o While value is TRUE stone is jumping
// o If value is FALSE stone id drowned

while ((bool)mt_rand(0, 1)) {
    echo 'Jump' . PHP_EOL;
}
echo 'Drowned';