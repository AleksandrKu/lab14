<?php
// Program displays numbers in digits and words from 1 to 100
// array numbers from 1 to 19
$number1to19 = array(
    '', 'один', 'два', 'три', 'четыре', 'пять', 'шесть',
    'семь', 'восемь', 'девять', 'десять', 'одиннадцать',
    'двенадцать', 'тринадцать', 'четырнадцать', 'пятнадцать',
    'шестнадцать', 'семнадцать', 'восемнадцать', 'девятнадцать');
// array decades numbers from 10 to 100
$number20to100 = array(
    '', '', 'двадцать', 'тридцать', 'сорок', 'пятьдесят',
    'шестьдесят', 'семьдесят', 'восемьдесят', 'девяносто', 'сто');

// Display decades numbers  20, 30, 40, 50, 60, 70, 80, 90, 100
function displayDecade($decade, $number20to100) // send number and array $number20to100 numbers from 20 to 100
{
    $arrayDecate = str_split($decade); // two-digit number  separate in an array
    $fistNumerDecate = intval($arrayDecate[0]);  // first number this array show decade
    if ($decade == 100) {  // if number 100 than return  $number20to100[10] = 'сто'
        $fistNumerDecate = 10;
    }
    return $number20to100[$fistNumerDecate];
}
// Display all two-digit numbers except round numbers ( 20,30,...)
function twoDigitNumbers($twoDigit, $number20to100, $number1to19)
{
    $arrayTwoDigit = str_split($twoDigit); // two-digit number  separate in an array
    $fistNumerTwoDigit = intval($arrayTwoDigit[0]); // first number is show decade
    $secondNumerTwoDigit = intval($arrayTwoDigit[1]); // second number
    return $twoDigitNumbersDisplay = $number20to100[$fistNumerTwoDigit] . " " . $number1to19[$secondNumerTwoDigit];
}
// Program displays numbers in digits and words from 1 to 100
for ($i = 1; $i <= 100; $i++) {
    echo $i;
    if ($i <= 19) {
        echo " = " . $number1to19[$i];  // numbers from 1 to 19
    } elseif (!($i % 10)) {
        echo " = " . displayDecade($i, $number20to100);  // decade numbers  20, 30, 40, 50, 60, 70, 80, 90, 100
    } else {
        echo " = " . twoDigitNumbers($i, $number20to100, $number1to19); // two-digit numbers 21,22, ... etc.
    }
    echo PHP_EOL;
}


