<?php
/*Student should create array with already created keys (md5 hashes of any quantity (4-6) numbers from 100 to 110)
* Student should create script which will generate unique (not existed in array with prepared keys) key.
* Key should by md5 hash of number sequence from 100 to 110
* Student should print each generated key (even existed)*/
// http://www.md5.cz/  create four hash
$hashes = [
    'f899139df5e1059396431415e770c6dd',
    'ec8956637a99787bd197eacd77acce5e',
    'c9e1074f5b3f9fc8ea15d152add07294',
    'f0935e4cd5920aa6c7c996a5ee53a70f'
];
$new_hashes = [];
$i = 100;
do {
    $current_hash = md5($i);  // Calculate the md5 hash of a string
    if(in_array($current_hash, $hashes)) {  //Checks if a $current_hash exists in an array $hashes
        echo "\n".$current_hash;  // hash from existing array
            } else {
        $new_hashes[] = $current_hash;
    }
    $i++;
} while ($i <= 110);
echo PHP_EOL.PHP_EOL;
echo implode(PHP_EOL, $new_hashes);
