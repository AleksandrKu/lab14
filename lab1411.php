<?php
// lab 1.4.1.1
// Функция калькулятор
function calculator($number1, $operation, $number2)
{
    // проверяем оператор на существование функцией empty
  if (empty($operation)) {
      echo  "Error: Empty operation!"; // если пустой, то выводим это сообщение
      return false;  // заканчиваем выполнение функциии возвращаем false
  }
    // аргументы должны быть чиcлами
  if (!is_numeric($number1) || !is_numeric($number2)) { // проверка значений на число это или нет
      echo  "Error: Bad number!";  // если хоть один аргумент не число, то выводим это сообщение
      return false; // заканчиваем выполнение функциии, возвращаем false
  }
    // если оператор существует и аргументы числа, то выполняем switch
  switch ( $operation) {
      case '+':
          return $number1+$number2; // оператор '+' складываем 2 значения и возвращаем результат
      case '-':
          return  $number1-$number2; // оператор '-' вычитаем из первого значения второй и возвращаем результат
      case '*':
          return  $number1*$number2; //  '*' умножаем и возвращаем результат
      case '/':
          if ($number2 == 0)  {    // если второе знаечение (делитель) 0, то выводим сообщение об ошибке
              echo "Error: Division by zero!";
              return false; // заканчиваем выполнение функциии возвращаем false
          }
          return  $number1/$number2; // '/' делим и возвращаем резальтат
      default:
          echo  "Error: Bad operation!"; // если оператором приходит значение отличное от - + * / , то выводится это сообщение
          return false; // заканчиваем выполнение функциии возвращаем false
  }
}
// $argv[0]  - первый элемент массива полученного из коммандной строки содержит имя файла запущенного скрипта
$number1 = isset($argv[1])? (int)$argv[1] : 0;  // $argv[1] первое значение, превращаем в integer,второй элемент массива, переданный из командной строки
$operation = isset($argv[2])? $argv[2] : 0; // $argv[2] оператор, третий элемент массива, переданный из командной строки
$number2 = isset($argv[3])? (int)$argv[3] : 0; // $argv[3] второе значение, превращаем в integer, четвертый элемент массива, переданный из командной строки

$result = calculator($number1, $operation, $number2);
if ($result === false) { // если функция возвращает false, то заканчиваем выполнение программы
    exit;
}
echo "{$number1} {$operation} {$number2} = {$result}"; // вывод результата

