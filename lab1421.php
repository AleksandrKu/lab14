<?php
$firstElement = 0;
$lastElement = 50;
$array = range($firstElement, $lastElement);  // Create array from numbers
shuffle($array);  // shuffle array
echo "Mixed array :\n".implode(", ",$array).PHP_EOL;  // Print mixed array
$arrayForPrint = [ $array[$firstElement] , $array[$lastElement] , count($array)]; // Create array: first,last elements, number of elements in array

echo "\nFist element, Last element, namber of elements: ".implode(", ",$arrayForPrint); // Print array

$string = "Student should create variable with some string Lorem ipsum for example"; // Create variable with some string
$arrayFromString = explode(" ", $string); // Break string by spaces (create array of worlds)
echo "\n\nArray from string: ".implode(", ", $arrayFromString); // Print array of words

echo "\n\nCreate two arrays in different ways:\n";
$firstArray =[];
array_push($firstArray, 'subtraction');  // Create array using array_push
array_push($firstArray, 'multiplication');
array_push($firstArray, 'division');
echo "First array: ".implode(", ", $firstArray); // Print first array
$secondArray =['addition', 'involution', 'evolution']; // Create array in classic way
echo "\nSecond array: ".implode(", ", $secondArray); // Print second array
$resultArray = array_merge($firstArray,$secondArray);  // Merge two arrays
echo "\nResulting array: ".implode(", ", $resultArray); // Print merged arrays