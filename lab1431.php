<?php
/* Student should to provide an interface for receiving the minimum and maximum range values
 Student should to find all even numbers from specified interval
 Student should to print founded numbers separated by commas*/
$numbersInput = $argv;
// Check enter two values
function empty_input($numbersInput)
{
    $lengthArray = count($numbersInput);  // count length entered array
    if ($lengthArray == 1) {             //   empty array
        return "Enter two numbers";
    } elseif ($lengthArray == 2) {       //  only one number
        return "Enter second number";
    }
    return "ok";  // entered two numbers
}
// check errors in the entered data
function check_errors($numbersInput)
{
    foreach ($numbersInput as $number_key => $numberValue) {
        if ($number_key == 0) { // skip fist value (there is file's name)
            continue;
        }
        if (!is_numeric($numberValue)) {  // Check: is it number?
            $error = "\nEnter only numbers!";
            return $error;
        }
        $arr1 = str_split($numberValue);
        if ($arr1[0] == '-') {  // Check: is the first character '-' ?
            $error = "\nEnter numbers without '-'";
            return $error;
        }
    }
    return "ok";
}
// compute even numbers
function even_numbers($numbersInput)
{
    $finalArray = [];
    if ($numbersInput[2] > $numbersInput[1]) { // check which number is bigger
        $start = $numbersInput[1];
        $finish = $numbersInput[2];
    } else {
        $start = $numbersInput[2];
        $finish = $numbersInput[1];
    }
    for ($i = $start; $i <= $finish; $i++) {
        if ($i%2 == 0) { //if even number then add to array
            $finalArray[] = $i;
        }
    }
    return $finalArray;  // return array from even numbers

}

$errorsInput = empty_input($numbersInput);  // function check empty value entered from console
if ($errorsInput != "ok") {
    echo $errorsInput;  // if values empty then display warning
} else {
    $checkErrors = check_errors($numbersInput); // check errors in value entered from console
    if ($checkErrors != "ok") { // if values have  errors then display warning
        echo $checkErrors;
    } else {
     $finalResult = even_numbers($numbersInput); // if no errors then runs function and find all even numbers
        $minimumNumber = $numbersInput[1]; // minimum range values
        $maximumNumber = $numbersInput[2];  // maximum range values
     echo "Fist number - ".$minimumNumber."\nLast number - ".$maximumNumber.PHP_EOL;
     echo implode(", ",$finalResult);  // even numbers from specified interval
    }
}