<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06.02.2017
 * Time: 12:06
 */
$numberFib = 6; // Fibonacci number
function fibonacci($number)
{
        if ($number <= 1) {
        $result = $number;
    } else {
        $y1 = fibonacci($number-1); // first previous number
        $y2 = fibonacci($number-2); // second previous number
        $result = $y1 + $y2; // result
        }
    return ($result);
}
echo "Fibonacci number of {$numberFib} = ".fibonacci($numberFib);

// ---------------------------------------------------------------------------------------------
echo "\n\nTernary operator: \n";
function fibonacci2($number)
{
return ($number <= 1) ? $number : fibonacci2($number-1) + fibonacci2($number-2);
}
echo "Fibonacci number of {$numberFib} = ".fibonacci2($numberFib);

// -----------------------------------------------------------------------------------------------
echo "\n\nUsing an Anonymous function: \n";
$result2 = function($numberFib) use (&$result2)
{
   return ($numberFib <= 1) ? $numberFib : $result2($numberFib-1) + $result2($numberFib-2);
};
echo "Fibonacci number of {$numberFib} = ".$result2($numberFib);

