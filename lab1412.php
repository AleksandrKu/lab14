<?php
// find student by nickname in the database
function find_student($username, $students) {
    if(isset($students[$username])) {  // determine if the nickname is set and is not NULL
        return $students[$username]; //  nickname exists into the database , the function return array with all information about student
    } else {
        echo "No such user!"; // if nickname doesn't exist then this message is displayed
        return false;   // function return false
    }
}

function get_student_info_string($students_info, $username){
  echo "Information about student with nickname '{$username}': \n";
    echo "Name: {$students_info['name']}\nAge: {$students_info['age']}\nGender: {$students_info['gender']}";
}

// database of students
$students = [
    'peter' => [
        'name' => "Peter",
        'age' => 25,
        'gender'=> "male"
    ],
    'vasya' => [
        'name' => "Vasya",
        'age' => 20,
        'gender'=> "male"
    ],
    'iren' => [
        'name' => "Ira",
        'age' => 21,
        'gender'=> "female"
    ]
];

$username = isset($argv[1]) ? $argv[1] : ''; // nickname passed from command line
$students_info = find_student($username,$students); // check nickname and return all information about this student

if (boolval($students_info)) { // if nickname exists then call function get_student_info_string for display all information about student
    get_student_info_string($students_info, $username);
}
