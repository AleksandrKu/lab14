<?php
// Student should split transferred parameters array by pairs
//  Student should check first parameter of each pair for valid key:
// Key must have any length and starts from two minuses (--)
//  Key must have two symbols length and starts from one minus (-)
//  Student should save all valid keys and their values to associative array

function process_params($params_array)
{
    $keyArray = [];
    foreach ($params_array as $param_key => $param_value) {
        if ($param_key == 0) continue; // end loop. In 0 position is file's name
        if (substr_count($param_value, '=') == 0) {  // Error if no '='
            echo "\nWrong parameter: {$param_value}. Add '=' ";
            continue; // end loop
        }
        if (substr_count($param_value, '=') > 1) { // Error if '=' more then one
            echo "\nWrong parameter: {$param_value}. Must be only one '=' ";
            continue;
        }
        $valueArray = explode("=", $param_value); // Split a string on key and value
        $keyParam = $valueArray[0];  // key
        $ValueParam = $valueArray[1];  // value
        if (substr_count($keyParam, '-') == 0) {  // count quantity (-)
            echo "\nWrong parameter: {$param_value}. Must be start with '-' "; // Error if before key no  (-)
            continue;
        } elseif (substr_count($keyParam, '-') == 1) { // if one (=) it is OK
            $key = mb_substr($keyParam, 1); // Get part of string start from 1 position. 0 is (-)
            if (strlen($key) != 2) { // must have two symbols length  if isn't - Error
                echo "\nKey must have two symbols length if start with one minus (-).";
                continue;
            }
            $keyArray[$key] = $ValueParam; // add in array key and value if before were one (-)
        }
        if (substr_count($keyParam, '-') == 2) { // is start with two (-)
            $key = mb_substr($keyParam, 2); // Get part of string start from 2 position. 0 and 1 are (-)
            $keyArray[$key] = $ValueParam; // add in array key and value   if before were two (-)
                    }
        }
    return $keyArray; // return associative array
}

$params_array = $argv; // transferred parameters array from console
$keyArrayPrint = process_params($params_array); //
foreach ($keyArrayPrint as $key => $value) {
    if (isset($value)) {  // if value isn't empty then print result
    echo " \nKey " . $key . " = " . $value; }
}