<?php
$requestByConsumerIn = $argv; // Withdrawal amount entered in the console
// $requestByConsumer = 1480;   // Withdrawal amount is determined in the program
echo "Withdrawal amount : ";
echo $requestByConsumer = $requestByConsumerIn[1];
 // Set up quantities of notes in the ATM
$arrayNotesInAtm = array(
    '100' => 10,
    '50' => 20,
    '20' => 50,
    '10' => 100,
);
// Calculation of the amount of money in the ATM
echo "\nIn the ATM = " . $amountOfMoneyInAtm = $arrayNotesInAtm['100'] * 100 + $arrayNotesInAtm['50'] * 50 + $arrayNotesInAtm['20'] * 20 + $arrayNotesInAtm['10'] * 10;

$arrayNumberNotesForOutput = array(
    '100' => 0,
    '50' => 0,
    '20' => 0,
    '10' => 0,
);

function checkErrors($requestByConsumer, $amountOfMoneyInAtm)
{
    if (!is_numeric($requestByConsumer)) {  // Check: is it number?
        echo "\nEntered only numbers!";
        return false;
    }
    $arr1 = str_split($requestByConsumer);
    if ($arr1[0] == '-') {  // Check: is the first character '-' ?
            echo "\nEntered only numbers without '-'!";
            return false;
        }

    if ($requestByConsumer > $amountOfMoneyInAtm) { // Amount of money to give more than in the ATM
        echo "\nNot enough cash in the ATM!\n";
        return false;
    } elseif ($requestByConsumer % 10 != 0) {  // Amount of money is not a multiple of 10
        echo "\nEnter withdrawal amount in multiples of 10!\n";
        return false;
    }
    return true;
}

//  Calculation  number of notes to give
function countNotesForOutput($requestByConsumer)
{
    $numbers = func_get_args(); //  Receive array with the number of notes in the ATM
    echo "\nNumber of 100grn. = " . $numbers['1']['100'];
    echo "\nNumber of 50grn. = " . $numbers['1']['50'];
    echo "\nNumber of 20grn. = " . $numbers['1']['20'];
    echo "\nNumber of 10grn. = " . $numbers['1']['10'];
    $arrayNumberNotesForOutput['100'] = 0;
    $arrayNumberNotesForOutput['50'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.
    $arrayNumberNotesForOutput['20'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.
    $arrayNumberNotesForOutput['10'] = 0; // Defined. If for give need only notes by 100grn this values will be undefined.

    // Calculation  how many notes by 100grn to need
    $arrayNumberNotesForOutput['100'] = intval($requestByConsumer / 100);
    $numbers['1']['100'] = $numbers['1']['100'] - $arrayNumberNotesForOutput['100']; // How many notes by 100grn left in ATM
    $remainder = intval($requestByConsumer % 100);  //  Remainder. Shows amount many left to give
    // If banknotes by 100grn are over the remainder is increased by this amount
    if ($numbers['1']['100'] < 0) {
        $remainder = $remainder - $numbers['1']['100'] * 100;
        $arrayNumberNotesForOutput['100'] = $arrayNumberNotesForOutput['100'] + $numbers['1']['100']; // Count the number of notes to give
    }
    // Calculation  how many notes by 50grn to need
    if ($remainder != 0) {
        $arrayNumberNotesForOutput['50'] = intval($remainder / 50);
        $numbers['1']['50'] = $numbers['1']['50'] - $arrayNumberNotesForOutput['50'];
        $remainder = intval($remainder % 50);
        if ($numbers['1']['50'] < 0) {
            $remainder = $remainder - $numbers['1']['50'] * 50;
            $arrayNumberNotesForOutput['50'] = $arrayNumberNotesForOutput['50'] + $numbers['1']['50'];
        }
        // Calculation  how many notes by 20grn to need
        if ($remainder != 0) {
            $arrayNumberNotesForOutput['20'] = intval($remainder / 20);
            $numbers['1']['20'] = $numbers['1']['20'] - $arrayNumberNotesForOutput['20'];
            $remainder = intval($remainder % 20);
            if ($numbers['1']['20'] < 0) {
                $remainder = $remainder - $numbers['1']['20'] * 20;
                $arrayNumberNotesForOutput['20'] = $arrayNumberNotesForOutput['20'] + $numbers['1']['20'];
            }

            // Calculation  how many notes by 10grn to need
            if ($remainder != 0) {
                $arrayNumberNotesForOutput['10'] = intval($remainder / 10);
                echo PHP_EOL;
                $remainder = intval($remainder % 10);
                echo $remainder != 0 ? "Error!  Enter other number" : " ";
            }
        }

}
    return $arrayNumberNotesForOutput;
}

// Count how many notes left in the ATM
function leftNoutesInATM($inATM, $outFromATM)
{
    return $inATM - $outFromATM;
}

if (checkErrors($requestByConsumer, $amountOfMoneyInAtm)) { // check errors
    $requestByConsumer = intval($requestByConsumer);
    $arrayNumberNotesForOutput = countNotesForOutput($requestByConsumer, $arrayNotesInAtm);  //  Calculation number of notes to give
    $arrayLeftNoutesInATM = array_map("leftNoutesInATM", $arrayNotesInAtm, $arrayNumberNotesForOutput); // Count how many notes left in the ATM
    echo PHP_EOL.PHP_EOL;
    echo <<<ENDD
ATM must gives:
100grn - $arrayNumberNotesForOutput[100] notes
50grn - $arrayNumberNotesForOutput[50] notes
20grn - $arrayNumberNotesForOutput[20] notes
10grn - $arrayNumberNotesForOutput[10] notes
ENDD;

echo PHP_EOL.PHP_EOL;
echo <<<LEFTT
Left in ATM:
100grn -$arrayLeftNoutesInATM[0] notes
50grn - $arrayLeftNoutesInATM[1] notes
20grn - $arrayLeftNoutesInATM[2] notes
10grn - $arrayLeftNoutesInATM[3] notes
LEFTT;
}